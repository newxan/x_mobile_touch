const CANVAS_ELEMENT_NAME = 'main-canvas'

const EVENT_MOUSE_DOWN = 'mousedown'
const EVENT_MOUSE_MOVE = 'mousemove'
const EVENT_MOUSE_OUT = 'mouseout'
const EVENT_MOUSE_UP = 'mouseup'
const EVENT_TOUCH_MOVE = 'touchmove'
const EVENT_TOUCH_START = 'touchstart'
const EVENT_TOUCH_STOP = 'touchstop'

const DEFAULT_LINE_JOIN = 'round'
const DEFAULT_LINE_CAP = 'round'
const DEFAULT_LINE_WIDTH = 10
const DEFAULT_STROKE_STYLE = '#BADA55'

const getElement = (elementName) => {
  return document.getElementById(elementName)
}

const getWindowWidth = () => {
  return window.innerWidth
}

const getWindowHeight = () => {
  return window.innerHeight
}

const extractCordinates = (event) => {
  let x = null
  let y = null
  switch(event.type) {
    case EVENT_MOUSE_DOWN:
    case EVENT_MOUSE_UP:
    case EVENT_MOUSE_MOVE:
    case EVENT_MOUSE_OUT:
      x = event.offsetX
      y = event.offsetY
      break
    case EVENT_TOUCH_MOVE:
    case EVENT_TOUCH_START:
    case EVENT_TOUCH_STOP:
      x = event.touches[0].clientX
      y = event.touches[0].clientY
      break
    default:
      throw new Error('Unexpected event type.')
      break
  }

  return { newX: x, newY: y }
}

let app = null

class App {
  constructor(width, height) {
    this.width = width
    this.height = height
    this.isDrawing = false
    this.lastX = 0
    this.lastY = 0
    this.hue = 0
    this.strokeStyle = DEFAULT_STROKE_STYLE
    this.lineJoin = DEFAULT_LINE_JOIN
    this.lineCap = DEFAULT_LINE_CAP
    this.lineWidth = DEFAULT_LINE_WIDTH
  }

  init() {
    this.canvas = getElement(CANVAS_ELEMENT_NAME)
    this.ctx = this.canvas.getContext('2d')
    this.canvas.width    = this.width
    this.canvas.height   = this.height
    this.ctx.strokeStyle = this.strokeStyle
    this.ctx.lineJoin    = this.lineJoin
    this.ctx.lineCap     = this.lineCap
    this.ctx.lineWidth   = this.lineWidth
    this.canvas.addEventListener(EVENT_MOUSE_DOWN, this.handleStartEvent.bind(this))
    this.canvas.addEventListener(EVENT_MOUSE_MOVE, this.handleMoveEvent.bind(this))
    this.canvas.addEventListener(EVENT_MOUSE_UP, this.handleStopEvent.bind(this))
    this.canvas.addEventListener(EVENT_MOUSE_OUT, this.handleStopEvent.bind(this))
    this.canvas.addEventListener(EVENT_TOUCH_START, this.handleStartEvent.bind(this))
    this.canvas.addEventListener(EVENT_TOUCH_MOVE, this.handleMoveEvent.bind(this))
    this.canvas.addEventListener(EVENT_TOUCH_STOP, this.handleStopEvent.bind(this))
  }

  setSize(width, height) {
    this.width = width
    this.height = height
    this.canvas.width = this.width
    this.canvas.height = this.height
  }

  draw(newX, newY) {
    this.ctx.strokeStyle = `hsl(${this.hue}, 100%, 50%)`
    this.ctx.beginPath()
    // start fromr
    this.ctx.moveTo(this.lastX, this.lastY)
    // go to
    this.ctx.lineTo(newX, newY)
    this.ctx.stroke()
    this.lastX = newX
    this.lastY = newY
    
    // changes the color every time you move the mouse
    this.hue++
    // resets hue back to 0 after reaching 360 max
    if(this.hue >= 360) {
      this.hue = 0
    }
  }

  handleStartEvent(e) {
    e.preventDefault();
    const { newX, newY } = extractCordinates(e)
    this.lastX = newX
    this.lastY = newY
    this.isDrawing = true
  }

  handleMoveEvent(e) {
    e.preventDefault();
    if (!this.isDrawing) {
      return
    }
    const { newX, newY } = extractCordinates(e)
    this.draw(newX, newY)
  }

  handleStopEvent(e) {
    e.preventDefault();
    const { newX, newY } = extractCordinates(e)
    this.lastX = newX
    this.lastY = newY
    this.isDrawing = false
  }
}


window.onload = () => {
  app = new App(getWindowWidth(), getWindowHeight())
  app.init()

  window.onresize = () => {
    app.setSize(window.innerWidth, window.innerHeight)
  }
}



